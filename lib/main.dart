import 'package:flutter/material.dart';
import 'core/utils/theme_utils.dart';
import 'core/utils/Router.dart';
import 'login/presentation/injection_login.dart' as s1;

void main() async
{
await s1.initdata();
WidgetsFlutterBinding.ensureInitialized();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      initialRoute:intialRoutepage,
      routes:buildRoutes,
      debugShowCheckedModeBanner:false,
      theme:login_theme,
    );
  }
}

