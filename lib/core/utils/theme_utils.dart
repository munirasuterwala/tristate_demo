
//login page theme
import 'package:flutter/material.dart';

ThemeData login_theme=ThemeData(
    primaryColor:Colors.white,
    accentColor:Colors.redAccent,
    floatingActionButtonTheme:FloatingActionButtonThemeData(elevation:0,
        backgroundColor:Colors.white
    ),
    textTheme:TextTheme(
        headline1:TextStyle(fontSize:22,color:Colors.redAccent),
        headline2:TextStyle(fontSize:24,color:Colors.redAccent,fontWeight:FontWeight.w700),
        bodyText1:TextStyle(fontSize:14,color:Colors.blueAccent,fontWeight:FontWeight.w400)
    ));