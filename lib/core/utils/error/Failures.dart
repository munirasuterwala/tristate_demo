abstract class Failures{
  Failures();
}
class ServerFailure implements Failures
{
  String message;
  ServerFailure(this.message);
}

class FailureMessage implements Failures
{
  String message;
  FailureMessage(this.message);
}
class CacheFailure implements Failures
{
  String message;
  CacheFailure(this.message);
}