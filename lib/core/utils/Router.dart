import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_tristate_demo/login/presentation/bloc/login_bloc.dart';
import 'package:flutter_tristate_demo/login/presentation/injection_login.dart';
import 'package:flutter_tristate_demo/login/presentation/pages/login_page.dart';

const intialRoutepage='/';
//const homePage='home';
//const detail='detail';

Map<String, WidgetBuilder> buildRoutes=
{

  intialRoutepage:(context)=>BlocProvider(
      create:(_)=>s1<LoginBloc>(),
      child: LoginPage()),
  //homePage:(context)=>WorldAppHomePage(),
  //chooseLocation:(context)=>ChooseLocation(context),
};