import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
class ProgessHud extends StatelessWidget
{
  final Widget child;
  final bool isAsyncCall;
  final double opacity;
  final Color color;
  final Animation<Color> valuecolor;

  ProgessHud(
  {
    this.child,
    this.isAsyncCall,
    this.opacity=0.3,
    this.color=Colors.grey,
    this.valuecolor
});

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    List<Widget> list=new List<Widget>();
    list.add(child);
    if(isAsyncCall)
      {
        var modal= Stack(
          children: [
            Opacity(opacity:opacity,
            child:ModalBarrier(dismissible:false,color:color,),),
            Center(
              child:CircularProgressIndicator(),
            )
          ],
        );
        list.add(modal);
      }
    return Stack(
      children:list,
    );
  }

}