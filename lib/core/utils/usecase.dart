import 'package:dartz/dartz.dart';
import 'error/Failures.dart';

abstract class Usecase<Type,Params>
{
  Stream<Either<Failures,Type>> call(Params params);
}