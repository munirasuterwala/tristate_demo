import 'package:dartz/dartz.dart';
import 'package:flutter_tristate_demo/core/utils/error/Failures.dart';
import 'package:flutter_tristate_demo/login/data/model/UserList.dart';

abstract class LoginRepository
{
  Stream<Either<Failures,List<UserList>>> getListUsers();
}