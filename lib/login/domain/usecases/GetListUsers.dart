
import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter_tristate_demo/core/utils/error/Failures.dart';
import 'package:flutter_tristate_demo/core/utils/usecase.dart';
import 'package:flutter_tristate_demo/login/data/model/UserList.dart';
import 'package:flutter_tristate_demo/login/domain/repository/login_repository.dart';

class GetListOfUsers extends Usecase<List<UserList>,NoParamData>
{
  LoginRepository repository;
  GetListOfUsers({this.repository});
  @override
  Stream<Either<Failures, List<UserList>>> call(params) {
    return repository.getListUsers();
  }
}



class NoParamData extends Equatable
{
  NoParamData();
  @override
  // TODO: implement props
  List<Object> get props => throw UnimplementedError();
}