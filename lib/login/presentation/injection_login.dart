import 'package:flutter_tristate_demo/login/data/datasources/login_remote_source.dart';
import 'package:flutter_tristate_demo/login/data/repositoryimpl/login_repository_impl.dart';
import 'package:flutter_tristate_demo/login/domain/repository/login_repository.dart';
import 'package:flutter_tristate_demo/login/domain/usecases/GetListUsers.dart';
import 'package:flutter_tristate_demo/login/presentation/bloc/login_bloc.dart';
import 'package:get_it/get_it.dart';

final s1=GetIt.instance;
Future<void> initdata() async
{
  //bloc
  s1.registerFactory(() => LoginBloc(getListOfUsers:s1()));

  //usecase
  s1.registerLazySingleton(() => GetListOfUsers(repository:s1()));

  //repository
  s1.registerLazySingleton<LoginRepository>(() =>LoginRepositoryImpl(remoteDataSource:s1()));

  //datasource
  s1.registerLazySingleton<LoginRemoteDataSource>(()=>LoginRemoteDataSourceImpl());
}