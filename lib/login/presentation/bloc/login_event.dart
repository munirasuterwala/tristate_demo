import 'package:equatable/equatable.dart';
import 'package:flutter_tristate_demo/login/data/model/UserList.dart';

abstract class BaseEvent extends Equatable
{
  BaseEvent();
}
 class GetUserListData extends BaseEvent
{
  GetUserListData();

  @override
  // TODO: implement props
  List<Object> get props => throw UnimplementedError();
}

class ErrorState extends BaseEvent
{
  ErrorState();

  @override
  // TODO: implement props
  List<Object> get props => throw UnimplementedError();
}


class EventInternetError extends BaseEvent
{
  String error;
  EventInternetError({this.error});
  @override
  List<Object> get props => new List();
}

class EventErrorGeneral extends BaseEvent
{
  String error;
  EventErrorGeneral({this.error});
  @override
  List<Object> get props => new List();
}

class GetDataList extends BaseEvent
{
  List<UserList> result;
  GetDataList(this.result);
  @override
  // TODO: implement props
  List<Object> get props => throw UnimplementedError();

}