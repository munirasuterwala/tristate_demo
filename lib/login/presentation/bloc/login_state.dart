import 'package:equatable/equatable.dart';
import 'package:flutter_tristate_demo/login/data/model/UserList.dart';

 class BaseState extends Equatable
{
  BaseState();

  @override
  // TODO: implement props
  List<Object> get props => [];
}
class GetDataUserLists extends BaseState
{
  List<UserList> getData;
  GetDataUserLists(this.getData);
}

class ErrorStateReturn extends BaseState
{
  ErrorStateReturn();

  @override
  // TODO: implement props
  List<Object> get props => throw UnimplementedError();
}



class EventErrorState extends BaseState
{
  String message;
  EventErrorState(this.message);
}

class InternetErrorState extends BaseState
{
  String message;
  InternetErrorState(this.message);
}