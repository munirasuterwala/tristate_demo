import 'dart:io';
import 'package:bloc/bloc.dart';
import 'package:dio/dio.dart';
import 'package:flutter_tristate_demo/core/utils/error/Failures.dart';
import 'package:flutter_tristate_demo/login/domain/usecases/GetListUsers.dart';

import 'login_event.dart';
import 'login_state.dart';

class LoginBloc extends Bloc<BaseEvent,BaseState>
{
  GetListOfUsers getListOfUsers;
  LoginBloc({this.getListOfUsers}) : super(BaseState());

  @override
  Stream<BaseState> mapEventToState(BaseEvent event) async*{
    if(event is GetUserListData)
    {
      print("data");
      await getresponse(event);
    }
    else if(event is GetDataList)
    {
      yield GetDataUserLists(event.result);
    }
    else if(event is EventErrorGeneral)
    {
      yield EventErrorState(event.error);
    }
    else if(event is EventInternetError)
    {
      yield InternetErrorState(event.error);
    }
    else if(event is ErrorState)
      {
        yield ErrorStateReturn();
      }
  }

  getresponse(GetUserListData event) {
    getListOfUsers.call(NoParamData()).listen((event) {
      event.fold((error) {
        if(error is FailureMessage)
          add(EventErrorGeneral(error:error.message));
        else if(error is IOException)
          add(EventInternetError(error:error.toString()));
        else if(error is DioError && error is SocketException)
          add(EventInternetError());
      }, (result) {
        add(GetDataList(result));
      } );
    });
  }

}