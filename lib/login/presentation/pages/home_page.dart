import 'package:flutter/material.dart';
import 'package:flutter_tristate_demo/login/data/model/UserList.dart';
import 'package:flutter_tristate_demo/login/presentation/pages/detail_screen.dart';
import 'package:url_launcher/url_launcher.dart';

class HomePage extends StatefulWidget {
  List<UserList> detaillist;
  HomePage(this.detaillist);

  @override
  _HomePageState createState() => _HomePageState(detaillist);
}

class _HomePageState extends State<HomePage> {

  List<UserList> detaillist;
  bool isLike=false;
  Color color=Colors.grey;
  List<bool> likes=[];
  _HomePageState(this.detaillist);
  @override
  Widget build(BuildContext context) {
    print("the list is ${detaillist}");
    return Scaffold(
      body:ListView.separated(
        itemBuilder:(_,int index)
    {

      return GestureDetector(
        onTap:()
        {
          var list=detaillist[index];
          print("the list is${list}");
          Navigator.push(context,MaterialPageRoute(builder:(context)=>DetailScreen(
            detaillist:list,
            oncallback:(value)
            {
              setState(()
              {
                detaillist[index].like=value;
              });
            },
          )));
        },
        child: Card(
            margin:EdgeInsets.fromLTRB(16, 16, 16, 0),
            elevation:2,
            color:Theme.of(context).accentColor,
            shadowColor:Colors.grey[600],
            shape:RoundedRectangleBorder(side:BorderSide(width:1,color:Colors.black,style:
            BorderStyle.solid)),
            child:Padding
              (
              padding:EdgeInsets.all(12),
              child:Column(
                crossAxisAlignment:CrossAxisAlignment.stretch,
                children: [
                  Text(detaillist[index].name,style:TextStyle(fontSize:18,color:Colors.grey[600]),),
                  SizedBox(height:6,),
                  InkWell(
                      onTap:() async
                    {
                      String url=detaillist[index].email;
                      if(await canLaunch(url))
                        {
                         await launch(url);
                        }
                      else
                        {
                          throw "Could not launch";

                        }
                    },
                      child: Text(detaillist[index].email,style:TextStyle(fontSize:14,color:Colors.grey[800]),)),
                  SizedBox(height:8,),
                  InkWell(
                      onTap:() async
                      {
                        String url=detaillist[index].phone;
                        if(await canLaunch(url))
                        {
                          await launch(url);
                        }
                        else
                        {
                          throw "Could not launch";
                        }
                      },
                      child: Text(detaillist[index].phone,style:TextStyle(fontSize:14,color:Colors.grey[800]),)),
                  FlatButton.icon(
                      onPressed:(){
                         likes=List.filled(detaillist.length,true);
                         if(!detaillist[index].like)
                         {
                           setState(() {
                             detaillist[index].like=!detaillist[index].like;
                           });
                         }
                         else
                           {
                             setState(() {
                               detaillist[index].like=!detaillist[index].like;
                             });
                           }

                  }, icon:Icon(Icons.favorite,
                      color: detaillist[index].like?Colors.red:Colors.grey
                      //color:likes[index]?Colors.grey:Colors.red
                      ),
                      label:Text("Like User"))
                ],
              ),
            )

        ),
      );
    },
    separatorBuilder:(_,index)=>Divider(),
    itemCount:detaillist.length
    ),
    );
  }
}
