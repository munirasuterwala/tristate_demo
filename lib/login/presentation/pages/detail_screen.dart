import 'package:flutter/material.dart';
import 'package:flutter_tristate_demo/login/data/model/UserList.dart';

class DetailScreen extends StatefulWidget {
  UserList detaillist;
  DetailScreen({this.detaillist,this.oncallback});
  Function oncallback;
  @override
  _DetailScreenState createState() => _DetailScreenState(this.detaillist,this.oncallback);
}

class _DetailScreenState extends State<DetailScreen> {

  UserList detail;
  var isLike=false;
  Color color;
  Function oncallback;
  var updateboolean;
  _DetailScreenState(this.detail,this.oncallback);
  @override
  Widget build(BuildContext context) {
    print("data is ${detail}");
    return Scaffold(
      appBar:AppBar(
        title:Text("Detail Page"),
        backgroundColor:Theme.of(context).accentColor,
      ),
      body: Card(
          elevation:2,
          color:Theme.of(context).primaryColor,
          shadowColor:Colors.grey[600],
          shape:RoundedRectangleBorder(side:BorderSide(width:1,color:Colors.black,style:
          BorderStyle.solid)),
          child:Padding
            (
            padding:EdgeInsets.all(12),
            child:Column(
              crossAxisAlignment:CrossAxisAlignment.stretch,
              children: [
                Text(detail.name,style:TextStyle(fontSize:18,color:Colors.grey[600]),),
                SizedBox(height:6,),
                Text(detail.email,style:TextStyle(fontSize:14,color:Colors.grey[800]),),
                SizedBox(height:8,),
                Text(detail.phone,style:TextStyle(fontSize:14,color:Colors.grey[800]),),
                FlatButton.icon(onPressed:(){
                  if(detail.like)
                  {
                    setState(() {
                      detail.like=!detail.like;
                      //updateboolean=!detail.like;
                    });
                  }
                  else
                  {
                    setState(() {
                      detail.like=!detail.like;
                      //updateboolean=!detail.like;
                    });
                  }
                  oncallback(detail.like);
                }, icon:Icon(Icons.favorite,color:detail.like?Colors.red:Colors.grey),
                    label:Text("Like User"))
              ],
            ),
          )

      ),
    );
  }
}
