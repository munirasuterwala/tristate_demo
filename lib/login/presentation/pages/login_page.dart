import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_tristate_demo/core/utils/ProgressHud.dart';
import 'package:flutter_tristate_demo/login/data/model/UserList.dart';
import 'package:flutter_tristate_demo/login/presentation/bloc/login_bloc.dart';
import 'package:flutter_tristate_demo/login/presentation/bloc/login_event.dart';
import 'package:flutter_tristate_demo/login/presentation/bloc/login_state.dart';
import 'package:flutter_tristate_demo/login/presentation/pages/home_page.dart';
import 'package:fluttertoast/fluttertoast.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {

  final scaffoldkey = GlobalKey<ScaffoldState>();

  //GlobalKey<FormFieldState> formkey = GlobalKey<FormFieldState>();
  GlobalKey<FormState> formKey = new GlobalKey<FormState>();
  bool hidePssword = true;
  //HttpService service = new HttpService();
  //LoginRequest request = new LoginRequest();
  bool isApiCall = false;
  TextEditingController emailcontt=new TextEditingController();
  TextEditingController passwcontt=new TextEditingController();
  List<UserList> list=[];

  @override
  void initState() {
    // TODO: implement initState
    BlocProvider.of<LoginBloc>(context).add(GetUserListData());
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return buildUI();
  }

  Widget buildUI()
  {
    return BlocListener(
      cubit: BlocProvider.of<LoginBloc>(context),
      listener:(context,state)
      {
        if(state is GetDataUserLists)
        {
          setState(() {
            isApiCall = false;
          });
          print("stateeee ");
          list=state.getData;
          print("the response is ${state.getData}");
          //if(list.isNotEmpty)
       //   {
            //var detaillist=list.where((data)=>data.email==emailcontt.text).toList();
           // Navigator.pushReplacement(context,MaterialPageRoute(builder:(context)=>HomePage(list)));
            // for(var data in list)
            //   {
            //     String username=data.username.substring(0,3);
            //     String phonenumber=data.phone;
            //     String validphonenumber=data.phone.substring(phonenumber.length-3);
            //     if(emailcontt.text!=data.email)
            //     {
            //
            //       BlocProvider.of<LoginBloc>(context).add(ErrorState());
            //     }
            //     //  if(passwcontt.text!=username && passwcontt.text!=validphonenumber)
            //     // {
            //     //   Fluttertoast.showToast(msg:"Password must be first 4 characters of username and last 4 characters of phone number");
            //     // }
            //     else
            //     {
            //       print("data valid");
            //     }
            // return Text("");
          }
        // else if(state is ErrorStateReturn)
        // {
        //   Fluttertoast.showToast(msg:"Email Doesn't Exist");
      },
      child:Scaffold(
        key: scaffoldkey,
        backgroundColor: Theme.of(context).accentColor,
        body: SingleChildScrollView(
          child: Column(
            children: [
              Container(
                width: MediaQuery.of(context).size.width,
                padding: EdgeInsets.symmetric(vertical: 30, horizontal: 20),
                margin: EdgeInsets.symmetric(vertical: 85, horizontal: 20),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(20),
                    color: Theme.of(context).primaryColor,
                    boxShadow: [
                      BoxShadow(
                          color: Theme.of(context).hintColor.withOpacity(0.2),
                          offset: Offset(0, 10),
                          blurRadius: 20)
                    ]),
                child: Form(
                  key: formKey,
                  child: Column(
                    children: [
                      SizedBox(
                        height: 25,
                      ),
                      Text(
                        "Login",
                        style: Theme.of(context).textTheme.headline2,
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      TextFormField(
                        controller:emailcontt,
                        showCursor:true,
                        //onSaved: (input) => request.email = input,
                        keyboardType: TextInputType.emailAddress,
                        validator: (input) =>
                        input.contains("@")
                            ? null
                            : "Email Id should be valid",
                        decoration: InputDecoration(
                            hintText: "Email Address",
                            prefixIcon: Icon(
                              Icons.email,
                              color: Theme
                                  .of(context)
                                  .accentColor,
                            ),
                            focusedBorder: UnderlineInputBorder(
                              borderSide: BorderSide(
                                color: Theme
                                    .of(context)
                                    .accentColor
                                    .withOpacity(0.2),
                              ),
                            ),
                            enabledBorder: UnderlineInputBorder(
                              borderSide: BorderSide(
                                color: Theme
                                    .of(context)
                                    .accentColor
                                    .withOpacity(0.2),
                              ),
                            )),
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      TextFormField(
                        //onSaved: (input) =>request.password = input,
                        keyboardType: TextInputType.text,
                        maxLength:8,
                        obscureText: hidePssword,
                        controller:passwcontt,
                        validator: (input) =>
                        input.isNotEmpty && input.length<8
                            ? null
                            : "Please Enter Password and of 8 characters",
                        decoration: InputDecoration(
                            hintText: "Password",
                            suffixIcon: IconButton(
                              onPressed: () {
                                setState(() {
                                  hidePssword = !hidePssword;
                                });
                              },
                              color: Theme
                                  .of(context)
                                  .accentColor
                                  .withOpacity(0.4),
                              icon: hidePssword
                                  ? Icon(Icons.visibility_off)
                                  : Icon(Icons.visibility),
                            ),
                            prefixIcon: Icon(
                              Icons.lock,
                              color: Theme
                                  .of(context)
                                  .accentColor,
                            ),
                            focusedBorder: UnderlineInputBorder(
                              borderSide: BorderSide(
                                color: Theme
                                    .of(context)
                                    .accentColor
                                    .withOpacity(0.2),
                              ),
                            ),
                            enabledBorder: UnderlineInputBorder(
                              borderSide: BorderSide(
                                color: Theme
                                    .of(context)
                                    .accentColor
                                    .withOpacity(0.2),
                              ),
                            )),
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      FlatButton(
                        shape: StadiumBorder(),
                        color: Colors.redAccent,
                        padding: EdgeInsets.symmetric(
                            vertical: 12, horizontal: 80),
                        onPressed:()
                        {
                          if(validateAndSave()) {
                            print("validation done");
                            // String username=data.username.substring(0,4);
                            for(var data in list)
                              {
                                String username=data.username.substring(0,4);
                                //String phonenumber=data.phone.substring(0,4).toLast();
                                String validphonenumber=data.phone.substring(data.phone.length-4);
                                print("the username isss $username");
                                print("theusername is ${passwcontt.text.substring(0,4)}");
                                print("the phone number is $validphonenumber");
                                print("the phone number is ${passwcontt.text.substring(passwcontt.text.length-4)}");
                              }

                            var listnew=list.where((data)=>data.email==emailcontt.text.toString()).toList();
                            var passwordnew=
                            list.where((data)=>data.username.substring(0,4)==passwcontt.text.substring(0,4)
                             && data.phone.substring(data.phone.length-4)
                                    ==passwcontt.text.substring(passwcontt.text.length-4)).toList();

                              if (listnew.length<1)
                              {

                                   print("the email is ${emailcontt.text}");
                                   Fluttertoast.showToast(
                                    msg: "Email Doesn't Exist");
                              }
                               else if(passwordnew.length<1)
                                      {
                                        Fluttertoast.showToast(msg:"Password must be first 4 characters of username and last 4 characters of phone number");
                                      }
                              else
                                {
                                  print("hehehhhhd");
                                  var data=list.removeWhere((data)=>data.email==emailcontt.text.toString());
                                  print("the length is ${list.length}");

                                Navigator.pushReplacement(context,
                                    MaterialPageRoute(builder: (context) =>
                                        HomePage(
                                           list
                                        )));
                              }

                          }
                          else
                            {
                              Fluttertoast.showToast(
                                  msg: "Please Enter Details");
                            }
                        },
                        child: Text("Login", style: TextStyle(color: Colors.white),),),
                    ],
                  ),
                ),
              )
            ],
          ),
        ),
      ),

  );
  }


  bool validateAndSave() {
    {
      print("data");
      final form = formKey.currentState;
      if (form.validate()) {
        print("truee");
        form.save();
        return true;
      }
      return false;
    }
  }
}
