import 'dart:convert';

import 'package:flutter_tristate_demo/login/data/model/UserList.dart';
import 'package:http/http.dart';

class HttpService
{
  //define main url;
  String mainUrl="https://jsonplaceholder.typicode.com/users";
  Future<List<UserList>> getDataUsers() async
  {
    Response response= await get(mainUrl);
    if(response.statusCode==200)
    {
      List<dynamic> body=jsonDecode(response.body);
      List<UserList> userlist=body.map((e) =>UserList.fromJson(e)).toList();
      return userlist;
    }
    else{
      throw "No Users";
    }
  }

}