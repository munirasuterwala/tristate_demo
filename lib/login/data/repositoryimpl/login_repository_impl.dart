import 'package:dartz/dartz.dart';
import 'package:flutter_tristate_demo/core/utils/error/Failures.dart';
import 'package:flutter_tristate_demo/login/data/datasources/login_remote_source.dart';
import 'package:flutter_tristate_demo/login/data/model/UserList.dart';
import 'package:flutter_tristate_demo/login/domain/repository/login_repository.dart';

class LoginRepositoryImpl extends LoginRepository
{
  LoginRemoteDataSource remoteDataSource;
  LoginRepositoryImpl({this.remoteDataSource});
  @override
  Stream<Either<Failures, List<UserList>>> getListUsers() {
    return remoteDataSource.getdataUsers();
  }

}