import 'package:dartz/dartz.dart';
import 'package:dio/dio.dart';
import 'package:flutter_tristate_demo/core/utils/error/Failures.dart';
import 'package:flutter_tristate_demo/login/data/apimanager/HttpService.dart';
import 'package:flutter_tristate_demo/login/data/model/UserList.dart';

abstract class LoginRemoteDataSource
{
  Stream<Either<Failures,List<UserList>>> getdataUsers();
}
class LoginRemoteDataSourceImpl extends LoginRemoteDataSource
{
  HttpService service=new HttpService();
  @override
  Stream<Either<Failures,List<UserList>>> getdataUsers() async*{
    try{
       HttpService service=new HttpService();
      var data=await service.getDataUsers();
      if(data!=null)
        yield Right(data);
    }
    catch(e)
    {
      if(e is DioError)
      {
        if(e.response.statusCode==400)
        {
          yield Left(FailureMessage(e.response.data.toString()));
        }
        if(e.response.statusCode==500)
        {
          yield Left(FailureMessage("Internal Server error"));
        }
        else{
          yield Left(FailureMessage(e.response.data.toString()));
        }
      }
    }
  }

}